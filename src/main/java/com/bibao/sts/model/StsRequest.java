package com.bibao.sts.model;

public class StsRequest {
	private String user;
	private int seconds;
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public int getSeconds() {
		return seconds;
	}
	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}
	
	@Override
	public String toString() {
		return "StsRequest [user=" + user + ", seconds=" + seconds + "]";
	}
	
}
