package com.bibao.sts.lambda;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.Credentials;
import com.amazonaws.services.securitytoken.model.GetSessionTokenRequest;
import com.amazonaws.services.securitytoken.model.GetSessionTokenResult;
import com.bibao.sts.model.StsRequest;
import com.bibao.sts.model.StsResponse;

public class StsFunctionHandler implements RequestHandler<StsRequest, StsResponse> {

    @Override
    public StsResponse handleRequest(StsRequest stsRequest, Context context) {
        context.getLogger().log("Access Request: " + stsRequest);
        
        String stsEndpoint = "https://sts.us-east-1.amazonaws.com";
        String region = "us-east-1";
        
        AWSSecurityTokenService stsClient = 
        		AWSSecurityTokenServiceClientBuilder.standard().withEndpointConfiguration(
        			new AwsClientBuilder.EndpointConfiguration(stsEndpoint, region))
        		.build();
        
        GetSessionTokenRequest tokenRequest = new GetSessionTokenRequest();
        tokenRequest.setDurationSeconds(stsRequest.getSeconds());
        
        GetSessionTokenResult result = stsClient.getSessionToken(tokenRequest);
        
        return mapToResponse(result.getCredentials(), stsRequest.getUser());
    }

    private StsResponse mapToResponse(Credentials creds, String user) {
    	StsResponse response = new StsResponse();
    	response.setAccessKeyId(creds.getAccessKeyId());
    	response.setSecretAccessKey(creds.getSecretAccessKey());
    	Date expiration = creds.getExpiration();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	response.setExpiration(sdf.format(expiration));
    	response.setUser(user);
    	response.setSessionToken(creds.getSessionToken());
    	return response;
    }
}
