package com.bibao.sts.lambda;

import org.junit.jupiter.api.Test;

import com.amazonaws.services.lambda.runtime.Context;
import com.bibao.sts.model.StsRequest;
import com.bibao.sts.model.StsResponse;

public class StsFunctionHandlerTest {
	private Context createContext() {
        TestContext ctx = new TestContext();
        ctx.setFunctionName("TrainingProfileFunction");

        return ctx;
    }
	
	@Test
    public void testStsFunctionHandler() {
		StsFunctionHandler handler = new StsFunctionHandler();
        Context ctx = createContext();
        StsRequest request = new StsRequest();
        request.setUser("dummy");
        request.setSeconds(1000);
        StsResponse response = handler.handleRequest(request, ctx);

        System.out.println("Access Key: " + response.getAccessKeyId());
        System.out.println("Secret Key: " + response.getSecretAccessKey());
        System.out.println("Secret Token: " + response.getSessionToken());
        System.out.println("Expiration: " + response.getExpiration());
    }
}
